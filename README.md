# VoidBsp

**Script para tener Bspwm listo para su uso en Void Linux**

---
[![2022-03-01-14-10-27-screenshot.png](https://i.postimg.cc/kgqRLXBH/2022-03-01-14-10-27-screenshot.png)](https://postimg.cc/HVvLym00)

---

Recuerda volverlo ejecutable con `chmod +x install`

Ejecutalo con: `./install`


---

Algunos Paquetes que contiene:

Panel: **Tint2**

Gestor de ventanas: **BSPWM** 

Hokeys Daemon: **Sxhkd** [(Ver atajos de Sxhkd)](https://gitlab.com/d33vliter/voidbsp/-/blob/master/config/sxhkd/sxhkdrc)

Navegador: **Firefox**

Gestor de Archivos: **PCManFM**

Extractor de Archivos: **file-roller**

Network Manager: **Nmtui**

Terminal: **Sakura**

Editor de Textos: **Leafpad y Vim**

Reproductor Multimedia: **MPV**

Administrador de dispositivos de audio: **Pulsemixer**

Applet de volumen: **Volumeicon**

Gestor de Wallpaper: **Nitrogen**

Cambiar Resolución de Pantalla: **Arandr**

Para cambiar apariencia: **Lxappearance**
